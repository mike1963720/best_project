interface GetNum {
  getNum(): number;
}

type MM = 'multiply' | 'divide' | 'multiply_with_unit';

class Multiplier {
  constructor(readonly getNum: GetNum) {}

  multiply(n: number) {
    return this.getNum.getNum() * n;
  }

  divide(n: number) {
    return this.getNum.getNum() / n;
  }

  multiply_with_unit(n: number, unit: string) {
    const res = this.getNum.getNum() * n;
    return `${res} ${unit}`;
  }

  static proxy(getNum: GetNum, methodName: MM) {
    return (...args: any[]) => {
      const mult = new Multiplier(getNum);
      const method = mult[methodName] as any;
      return method(...args);
    };
  }
}

class BigNumGetter {
  getNum() {
    return 100;
  }

  multiply(n: number) {
    return new Multiplier(this).multiply(n);
  }
}

class SmallNumGetter {
  getNum() {
    return 1;
  }

  multiply = Multiplier.proxy(this, 'multiply');
}
